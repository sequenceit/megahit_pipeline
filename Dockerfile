FROM ubuntu:16.04

RUN apt-get update

# Install programs needed for the installations 
RUN apt-get install -y build-essential unzip wget software-properties-common python-setuptools 

# Install Megahit
RUN mkdir -p /opt/metagenomic
WORKDIR /opt/metagenomic
COPY metagenomic_pipeline.sh /opt/metagenomic/
COPY *.fastq /opt/metagenomic/

RUN wget https://github.com/voutcn/megahit/releases/download/v1.2.2-beta/MEGAHIT-1.2.2-beta-Linux-static.tar.gz -P /opt/metagenomic
RUN tar zvxf /opt/metagenomic/MEGAHIT-1.2.2-beta-Linux-static.tar.gz

# Install Bowtie2
RUN wget https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.3.1/bowtie2-2.3.3.1-linux-x86_64.zip/download -P /opt/metagenomic
RUN unzip /opt/metagenomic/download

# Install Samtools
RUN apt-get -y install libbz2-dev
RUN apt-get -y install zlib1g-dev
RUN apt-get -y install libncurses5-dev 
RUN apt-get -y install libncursesw5-dev
RUN apt-get -y install liblzma-dev

RUN wget https://github.com/samtools/htslib/releases/download/1.9/htslib-1.9.tar.bz2 -P /opt/metagenomic
RUN tar -vxjf /opt/metagenomic/htslib-1.9.tar.bz2
WORKDIR /opt/metagenomic/htslib-1.9
RUN make
WORKDIR /opt/metagenomic

RUN wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 -P /opt/metagenomic
RUN tar -vxjf /opt/metagenomic/samtools-1.9.tar.bz2
WORKDIR /opt/metagenomic/samtools-1.9
RUN make
WORKDIR /opt/metagenomic

# Install Metabat
RUN apt-get install -y scons libboost-all-dev libz-dev libbam-dev
RUN wget https://bitbucket.org/berkeleylab/metabat/downloads/metabat-static-binary-linux-x64_v0.25.4.tar.gz -P /opt/metagenomic  
RUN tar xzvf /opt/metagenomic/metabat-static-binary-linux-x64_v0.25.4.tar.gz 

# Install miniconda (to get hmmer, prodigal and pplacer) for checkM
RUN wget http://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
RUN bash Miniconda-latest-Linux-x86_64.sh -p /miniconda -b
RUN rm Miniconda-latest-Linux-x86_64.sh
ENV PATH=/miniconda/bin:${PATH}
RUN conda update -y conda
RUN conda install -c bioconda pplacer 
RUN conda install -c bioconda hmmer 
RUN conda install -c bioconda prodigal 

# CheckM relies on a number of precalculated data files which are downloaded
RUN wget https://data.ace.uq.edu.au/public/CheckM_databases/checkm_data_2015_01_16.tar.gz -P /opt/metagenomic/checkm
RUN tar -C /opt/metagenomic/checkm -xzvf /opt/metagenomic/checkm/checkm_data_2015_01_16.tar.gz

# Install checkM
RUN apt-get install -y python-pip
RUN pip install --upgrade pip 
RUN pip install numpy
RUN pip install checkm-genome
# temp. solution to the 'checkm data setRoot' ERROR
RUN echo -e "cat << EOF\n/opt/metagenomic/checkm\nEOF\n" | checkm data setRoot

ENTRYPOINT ["/opt/metagenomic/metagenomic_pipeline.sh"]
CMD ["*R1*.fastq", "*R2*.fastq", "1"]


