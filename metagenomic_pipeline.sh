#!/usr/bin/env bash

# Variables
MEM=232000000000

# Print some informative error meassages
err() {
	echo "$1 exited unexpectedly";
#	break;
}

# Function for checking the exit code of a child process
checkExit() {
if [ "$1" == "0" ]; then
	echo "[Done] $2 `date`";
	else
	err "[Error] $2 returned non-0 exit code $1";
	fi
}


	FILE1=$1
	FILE2=$2
	NSLOTS=$3

	# Megahit assembly
	/opt/metagenomic/MEGAHIT-1.2.2-beta-Linux-static/bin/megahit -m $MEM -1 $FILE1 -2 $FILE2 -t $NSLOTS -o Megahit --out-prefix megahit 2> megahit.log
		checkExit $? "megahit"

	# Bowtie2 mapping
	mkdir Bowtie2

	REF=Megahit/megahit.contigs.fa
	DB=Bowtie2/contigs
	/opt/metagenomic/bowtie2-2.3.3.1-linux-x86_64/bowtie2-build --threads $NSLOTS $REF $DB 2> bowtie2.log
		checkExit $? "bowtie2-build"

	/opt/metagenomic/bowtie2-2.3.3.1-linux-x86_64/bowtie2 -x $DB --no-unal --very-sensitive -p $NSLOTS --mm \
		-1 $FILE1 -2 $FILE2 -S Bowtie2/bowtie2.sam 2> bowtie2.log
		checkExit $? "bowtie2"

	/opt/metagenomic//samtools-1.9/samtools view -Sb Bowtie2/bowtie2.sam > Bowtie2/bowtie2.bam
		rm Bowtie2/bowtie2.sam
		checkExit $? "samtools"
	/opt/metagenomic/samtools-1.9/samtools sort Bowtie2/bowtie2.bam -o Bowtie2/bowtie2_sorted.bam
		rm Bowtie2/bowtie2.bam
		checkExit $? "samtools"

	# MetaBat binning
	mkdir Metabat
	/opt/metagenomic/metabat/jgi_summarize_bam_contig_depths --outputDepth Bowtie2/read_depth.txt \
					Bowtie2/bowtie2_sorted.bam 2> metabat.log
		checkExit $? "jgi_summarize_bam_contig_depths"
	
	/opt/metagenomic/metabat/metabat -i $REF -a Bowtie2/read_depth.txt -o Metabat/bin \
		--sensitive -t $NSLOTS --saveCls --unbinned --keep 2> metabat.log
		checkExit $? "metabat"

	checkm lineage_wf -t $NSLOTS -x fa Metabat CheckM
		checkExit $? "checkm"

	# Mark the sample as finished
	touch pipeline.done


