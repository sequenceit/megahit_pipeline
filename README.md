# Metagenomic pipeline
___

**Build:**  
*# Builds in cwd*  
`sudo docker build --tag=test .`  

**Run**:  
`sudo docker run test:latest`  

**List containers:**  
`sudo docker container ls --all`  

**Copy output from Dockerfile to a destination:**  
`sudo docker cp -a container_name:path/to/source path/to/destination`

`-a` = Archive mode (copy all information)  

**Get log file:**   
`sudo docker logs --details container_name`

___  

## Dockerfile   

The metagenomic pipeline takes input:  

`[FORWARD_FASTQ] [REVERSE_FASTQ] [NSLOTS]`  

There seems to be a known problem to dockerize checkm

``` 
ERROR:
It seems that the CheckM data folder has not been set yet or has been removed. Running: 'checkm data setRoot'.
Where should CheckM store it's data?
Please specify a location or type 'abort' to stop trying:
```  

It have been solved temporary with:  

`RUN echo -e "cat << EOF\n/opt/metagenomic/checkm\nEOF\n" | checkm data setRoot`  

Dependencies for checkm is installed using `miniconda` and checkm is install using `pip install` 
